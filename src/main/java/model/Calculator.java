package model;

import java.util.Locale;

public class Calculator {
    public static String execute(String[] params){
        String result;

            switch (params[0]) {
                case ("+"):
                    try {
                        if (params[1].equals(String.valueOf(Integer.MAX_VALUE)) || params[2].equals(String.valueOf(Integer.MAX_VALUE)) ||
                                params[1].equals(String.valueOf(Integer.MIN_VALUE)) || params[2].equals(String.valueOf(Integer.MIN_VALUE))){
                            throw new CalculatorException();
                        }
                        int x = Integer.parseInt(params[1]);
                        int y = Integer.parseInt(params[2]);
                        result = Integer.toString(x + y);
                        break;
                    } catch (Exception e){
                        throw new CalculatorException();
                    }

                case ("-"):
                    try {
                        double x1 = Double.parseDouble(params[1]);
                        double y1 = Double.parseDouble(params[2]);
                        result = Double.toString(x1 - y1);
                        break;
                    } catch (Exception e) {
                        throw new CalculatorException();
                }

                case ("*"):
                    try {
                        double x2 = Double.parseDouble(params[1]);
                        double y2 = Double.parseDouble(params[2]);
                        result = Double.toString(x2 * y2);
                        break;
                    } catch (Exception e) {
                        throw new CalculatorException();
                    }

                case ("/"):
                    if (params[2].equals("0") || params[2].equals("-0") || params[2].equals("0.0") ||
                            params[2].equals("-0.0")) {
                        throw new CalculatorException();
                    }
                    try {
                        double x3 = Double.parseDouble(params[1]);
                        double y3 = Double.parseDouble(params[2]);
                        result = String.format(Locale.ROOT, "%.3f", x3 / y3);
                        break;
                    } catch (Exception e) {
                        throw new CalculatorException();
                    }
                default: throw new CalculatorException();
            }
            return result;
    }
}