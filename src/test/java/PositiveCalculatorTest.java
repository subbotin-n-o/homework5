import model.Calculator;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class PositiveCalculatorTest {

    @DataProvider
    public Object[][] positiveData(){
        return new Object[][]{
                {"+", "2", "3", "5"},
                {"-", "-0.0", "-2.4", "2.4"},
                {"*", "2", "4.8", "9.6"},
                {"/", "2", "3", "0.667"}
        };
    }

    @Test (dataProvider = "positiveData")
    public void positiveTest(String znak, String zn1, String zn2, String result){
        Assert.assertEquals(Calculator.execute(new String[] {znak, zn1, zn2}), result, "Значения не равны");
    }
}