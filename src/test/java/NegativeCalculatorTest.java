import model.Calculator;
import model.CalculatorException;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class NegativeCalculatorTest {

    @DataProvider
    public Object[][] negativeData(){
        return new Object[][]{
                {"/", "1", "0"},
                {"/", "-1.1", "-0.0"},
                {"+", null, null},
                {"-", "one", "two"},
                {"plus", "1", "1"},
                {"+", "2147483647", "1"},
                {"+", "-2147483648", "-1"}
        };
    }

    @Test (dataProvider = "negativeData", expectedExceptions = {CalculatorException.class})
    public void negativeTest(String znak, String zn1, String zn2){
        Calculator.execute(new String[] {znak, zn1, zn2});
    }
}